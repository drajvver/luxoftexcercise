﻿namespace LuxoftTask.Domain.Extensions
{
    public static class StringExtensions
    {
        public static string ReverseRoundBracket(this string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return "";

            var res = "";

            for (var i = value.Length - 1; i > -1; i--)
            {
                switch (value[i])
                {
                    case '(':
                        res += ")";
                        break;
                    case ')':
                        res += "(";
                        break;
                    default:
                        res += value[i];
                        break;
                }
            }
            
            return res;
        }
    }
}