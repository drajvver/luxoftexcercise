﻿using System;
using LuxoftTask.Domain.Concrete;

namespace LuxoftTask
{
    class Program
    {
        static void Main(string[] args)
        {
            var handler = new TextManipulationHandler();
            var strings = new string[]
            {
                "Tests of a (sample) supplied by MeMFIS",
                "Tests of a sample [supplied by MeMFIS]",
                "Tests of [a (sample supplied)] by MeMFIS",
                "Tests of [a ((sample) supplied)] by MeMFIS"
            };
            foreach (var s in strings)
            {
                Console.WriteLine(handler.HandleString(s));
            }
            Console.Read();
        }
    }
}