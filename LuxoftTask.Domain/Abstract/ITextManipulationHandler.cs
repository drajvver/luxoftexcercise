﻿namespace LuxoftTask.Domain.Abstract
{
    public interface ITextManipulationHandler
    {
        string HandleRound(string phrase);
        string HandleSquare(string phrase);
        string HandleString(string value);
    }
}