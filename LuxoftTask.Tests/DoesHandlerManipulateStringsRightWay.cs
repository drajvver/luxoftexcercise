﻿using System;
using LuxoftTask.Domain.Abstract;
using LuxoftTask.Domain.Concrete;
using NUnit.Framework;

namespace LuxoftTask.Tests
{
    [TestFixture]
    public class DoesHandlerManipulateStringsRightWay
    {
        private readonly ITextManipulationHandler _handler;

        private readonly string[] _strings = new string[]
        {
            "Tests of a (sample) supplied by MeMFIS",
            "Tests of a sample [supplied by MeMFIS]",
            "Tests of [a (sample supplied)] by MeMFIS",
            "Tests of [a ((sample) supplied)] by MeMFIS"
        };
        
        public DoesHandlerManipulateStringsRightWay()
        {
            _handler = new TextManipulationHandler();
        }

        [Test]
        public void FirstSentenceOk()
        {           
            Assert.AreEqual("Tests of a elpmas supplied by MeMFIS", _handler.HandleString(_strings[0]));
        }
        
        [Test]
        public void SecondSentenceOk()
        {            
            Assert.AreEqual("Tests of a sample by MeMFIS supplied", _handler.HandleString(_strings[1]));
        }
        
        [Test]
        public void ThirdSentenceOk()
        {            
            Assert.AreEqual("Tests of a deilppus elpmas by MeMFIS", _handler.HandleString(_strings[2]));
        }
        
        [Test]
        public void FourthSentenceOk()
        {            
            Assert.AreEqual("Tests of a deilppus sample by MeMFIS", _handler.HandleString(_strings[3]));
        }
    }
}