﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using LuxoftTask.Domain.Abstract;
using LuxoftTask.Domain.Extensions;

namespace LuxoftTask.Domain.Concrete
{
    public class TextManipulationHandler : ITextManipulationHandler
    {
        public string HandleRound(string phrase)
        {
            if (string.IsNullOrWhiteSpace(phrase))
                return "";
            var chars = phrase.ReverseRoundBracket();
            return chars;
        }

        public string HandleSquare(string phrase)
        {
            if (string.IsNullOrWhiteSpace(phrase))
                return "";
            var words = phrase.Split(' ');
            words = words.OrderBy(t => t).ToArray();
            return string.Join(" ", words);
        }

        public string HandleString(string value)
        {
            MatchCollection wordsInsideRoundBrackets;

            do
            {
                wordsInsideRoundBrackets = Regex.Matches(value, "(?<=\\().*(?=\\))");
                foreach (Match match in wordsInsideRoundBrackets)
                {
                    var r = HandleRound(match.Value);
                    value = value.Replace($"({match.Value})", r);
                }
            } while (wordsInsideRoundBrackets.Count > 0);
            
            MatchCollection wordsInsideSquareBrackets;

            do
            {
                wordsInsideSquareBrackets = Regex.Matches(value, "(?<=\\[).+?(?=\\])");
                foreach (Match match in wordsInsideSquareBrackets)
                {
                    var r = HandleSquare(match.Value);
                    value = value.Replace($"[{match.Value}]", r);
                }
            } while (wordsInsideSquareBrackets.Count > 0);

            return value;
        }
    }
}