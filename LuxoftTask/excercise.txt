Exercise
Please write console application to transform sentences. Application should reverse the order of
characters when word or sentence are in round bracket and sort ascending words when sentence is in
square bracket. Please first execute function for round brackets.
Examples:
1. Sentence: Tests of a (sample) supplied by MeMFIS
Result: Tests of a elpmas supplied by MeMFIS.
2. Sentence: Tests of a sample [supplied by MeMFIS]
Result: Tests of a sample by MeMFIS supplied
3. Sentence: Tests of [a (sample supplied)] by MeMFIS
Result: Tests of a deilppus elpmas by MeMFIS
4. Sentence: Tests of [a ((sample) supplied)] by MeMFIS
Result: Tests of a deilppus sample by MeMFIS

Prepare solution based on C# console application, OOP, design patterns, nUnit tests.